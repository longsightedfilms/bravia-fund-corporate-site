<?php

return [
  'invest-pr-fitness-title' => 'Бесплатные абонементы на занятия в любом фитнес-клубе Вашего города',
  'invest-pr-fitness-desc' => 'Даже если Вы уже занимаетесь в каком-то фитнес-клубе, Вы можете сделать оплату абонементов бесплатной для себя',
  'invest-pr-fitness-list-1-title' => 'Бесплатные абонементы',
  'invest-pr-fitness-list-1-text' => 'Вам не нужно каждый месяц отдавать часть средств на оплату абонементов. Вы можете тратить эти деньги на себя или копить деньги на что-то.',
  'invest-pr-fitness-list-2-title' => 'Фитнес-клуб любого уровня',
  'invest-pr-fitness-list-2-text' => 'Вы можете позволить себе занятия в фитнес-клубе любого уровня в Вашем городе, ведь Вам не придётся платить за них.',
  'invest-pr-fitness-block-1-title' => 'Разместите депозит в Bravia Fund и мы возьмём на себя все вопросы по оплате Ваших абонементов',
  'invest-pr-fitness-block-1-desc' => 'Предоставьте нам реквизиты своего фитнес-клуба и мы ежемесячно будем пересылать оплату вместо Вас.',
  'invest-pr-fitness-list-3-text' => 'Выберите любой понравившийся фитнес-клуб в Вашем городе либо остаётесь заниматься в нынешнем.',
  'invest-pr-fitness-list-4-text' => 'В калькулятор, приведённом ниже, вводите стоимость абонемента.',
  'invest-pr-fitness-list-5-text' => 'Выбираете удобный вариант размещения депозита в соответствующей сумме в Bravia Fund.',
  'invest-pr-fitness-list-6-text' => 'Размещаете депозит.',
  'invest-pr-fitness-list-7-text' => 'Bravia Fund ежемесячно оплачивает занятия вместо Вас.',
  'invest-pr-fitness-block-left-1-text' => 'Вы заключаете договор с выбранным фитнес-клубом на стандартных условиях. Кладёте деньги на депозит в Bravia Fund и получаете ежемесячные выплаты, которые мы отправляем напрямую в счет оплаты Вашего абонемента. Сделка с Bravia Fund закреплена официальным договором.',
  'invest-pr-fitness-block-left-2-text' => 'Вы имеете возможность досрочно вывести депозит, не дожидаясь истечения срока по договору. В этом случае сумма будет возвращена Вам незамедлительно и в полном объеме за вычетом процентов, выплаченных за этот срок по депозиту в счет оплаты абонемента.',
  'invest-pr-fitness-calc-desc' => 'Установите стоимость абонемента в выбранном фитнес-клубе:',
  'invest-pr-fitness-calc-min' => '10000',
  'invest-pr-fitness-calc-max' => '300000',
  'invest-pr-fitness-calc-step' => '10000',
  'invest-pr-fitness-calc-default' => '180 000',
  'invest-pr-fitness-calc-percent-1' => '35',
  'invest-pr-fitness-calc-percent-2' => '40',
  'invest-pr-fitness-list-8-text' => 'Выбираете понравившийся фитнес-клуб и узнаете условия покупки абонемента на любой срок.',
  'invest-pr-fitness-list-9-text' => '<a href="https://my.bravia.fund/register">Регистрируетесь в Bravia Fund</a> и получаете доступ к личному кабинету.',
  'invest-pr-fitness-list-10-text' => 'Размещаете депозит в Bravia Fund на удобный Вам срок.',
  'invest-pr-fitness-list-11-text' => 'Предоставляете Bravia Fund реквизиты фитнес-клуба и Фонд оплачивает абонемент за Вас.',
  'invest-pr-fitness-note' => 'Оформление займет минимум времени, но главное - это выгодный и удобный способ заниматься в том фитнес-клубе, который Вам нравится',
  'invest-pr-fitness-block-2-title' => 'Вы можете заниматься бесплатно',
  'invest-pr-fitness-block-2-desc' => 'Выбрав любой фитнес-клуб Вашего города или воспользовавшись списком наших проверенных партнёров:'
];