<?php

return [
  'invest-pr-auto-title' => 'Бесплатный автомобиль любой марки в автосалонах Вашего города',
  'invest-pr-auto-desc' => 'Купите автомобиль Вашей мечты бесплатно',
  'invest-pr-auto-list-1-title' => 'Бесплатный автомобиль',
  'invest-pr-auto-list-1-text' => 'Вы можете купить автомобиль любой марки в автосалоне Вашего города бесплатно.',
  'invest-pr-auto-list-2-title' => 'Автоматическая выплата кредита',
  'invest-pr-auto-list-2-text' => 'Каждый месяц на Ваш счёт поступает сумма, равная платежу по кредиту за Ваш автомобиль.',
  'invest-pr-auto-block-1-title' => 'Разместите депозит в Bravia Fund и получайте ежемесячную выплату, равную погашению кредита',
  'invest-pr-auto-block-1-desc' => 'Любой автомобиль может стать Вашим бесплатно',
  'invest-pr-auto-list-3-text' => 'Выберите автомобиль любой марки в автосалонах Вашего города.',
  'invest-pr-auto-list-4-text' => 'Узнайте условия покупки машины в кредит (для физических лиц) или лизинг (для юридических лиц) в банке по вашему выбору и на его условиях.',
  'invest-pr-auto-list-5-text' => 'В нижеприведённом калькуляторе введите стоимость выбранного автомобиля.',
  'invest-pr-auto-list-6-text' => 'Разместите депозит на выбранный срок в Bravia Fund.',
  'invest-pr-auto-list-7-text' => 'После выплаты кредита выведите свой депозит.',
  'invest-pr-auto-note-1' => 'К окончанию срока депозита Вы будете иметь новый автомобиль и начальную сумму в полном размере.',
  'invest-pr-auto-block-left-1-text' => 'Вы заключаете договор на выдачу кредита с выбранным банком на стандартных условиях. Кладёте деньги на депозит в Bravia Fund и получаете ежемесячные выплаты, равные сумме платы за кредит. Сделка с Bravia Fund закреплена официальным договором.',
  'invest-pr-auto-block-left-2-text' => 'Вы имеете возможность досрочно вывести депозит, не дожидаясь истечения срока по договору. В этом случае сумма будет возвращена Вам незамедлительно и в полном объеме за вычетом процентов, выплаченных за этот срок по депозиту в счет оплаты кредита.',
  'invest-pr-auto-calc-desc' => 'Размер платежа по кредиту за автомобиль',
  'invest-pr-auto-calc-min' => '10000',
  'invest-pr-auto-calc-max' => '300000',
  'invest-pr-auto-calc-step' => '10000',
  'invest-pr-auto-calc-default' => '180 000',
  'invest-pr-auto-list-8-text' => 'Заключаете кредитный договор с салоном на покупку выбранного автомобиля.',
  'invest-pr-auto-list-9-text' => '<a href="https://my.bravia.fund/register">Регистрируетесь в Bravia Fund</a> и получаете доступ к личному кабинету.',
  'invest-pr-auto-list-10-text' => 'Размещаете депозит в Bravia Fund на удобный Вам срок.',
  'invest-pr-auto-list-11-text' => 'Получаете ежемесячную выплату равную платежу по кредиту.',
  'invest-pr-auto-note-2' => 'Оформление займет минимум времени, но главное - это выгодный и удобный способ купить тот автомобиль, который Вы хотите.'
];