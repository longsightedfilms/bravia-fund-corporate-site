﻿<?php

return [
  'card-title'=>'Bravia Fund card',
  'card-desc-1'=>'MasterCard card from our European partner bank. It is anonymous and can be used at any ATM or payment terminal of Russia and other countries',
  'card-desc-2'=>'For greater convenience and privacy, you can order a Mastercard of the European bank at us and transfer your income to it',
  'card-block-l-title'=>'Card the European bank will enable you to:',
  'card-block-l-list-1-text'=>'Receive income from deposits opened in the fund to your new card.',
  'card-block-l-list-2-text'=>'Make purchases in foreign stores without any restrictions.',
  'card-block-l-list-3-text'=>'Travel and spend money without paying a fee for conversion of USD to EUR.',
  'card-block-l-list-4-text'=>'Withdraw cash from ATMs around the world.',
  'card-block-l-list-5-text'=>'Use your European card account when making major purchases or receiving money from your partners.',
  'card-block-r-title'=>'We will open a card and account for you in one of our European partner banks for free:',
  'card-block-r-note'=> 'Card issue term - 1 month.'
];
