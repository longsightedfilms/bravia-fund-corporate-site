﻿<?php

return [
  'invest-pr-kasko-title' => 'Fully comprehensive insurance for your vehicle gratis',
  'invest-pr-kasko-desc' => 'Make your annual payments for fully comprehensive insurance free for you',
  'invest-pr-kasko-list-1-title' => 'Free vehicle insurance',
  'invest-pr-kasko-list-1-text' => 'You do not have to spend a share of your funds for the insurance annually. You can spend this money for yourself or save it up for something.',
  'invest-pr-kasko-block-1-title' => 'Open a deposit at Bravia Fund and we will take care of all issues related to payment.',
  'invest-pr-kasko-block-1-desc' => 'Provide us with bank details of and we will transfer your insurance payment for you',
  'invest-pr-kasko-list-2-text'=> 'Enter the insurance rate for your vehicle in the calculator below. ',
  'invest-pr-kasko-list-3-text' => 'Choose a preferred deposit option and amount in Bravia Fund.',
  'invest-pr-kasko-list-4-text' => 'Open a deposit.',
  'invest-pr-kasko-list-5-text' => 'Bravia Fund will pay for your insurance annually. ',
  'invest-pr-kasko-block-left-1-text' => 'You conclude a contract with the insurance company on standard terms. You replenish your deposit at Bravia Fund and receive monthly payments which we will transfer for paying of your insurance. The deal with Bravia Fund is documented by the official contract.',
  'invest-pr-kasko-block-left-2-text' => 'You can withdraw your deposit ahead of time without waiting for expiration of the contract. In this case, you will immediately receive the amount in full, after deduction of the interest used during the deposit period to pay for the vehicle insurance.',
  'invest-pr-kasko-calc-desc'=> 'Set the rate of your vehicle insurance:',
  'invest-pr-kasko-calc-min' => '500',
  'invest-pr-kasko-calc-max' => '100000',
  'invest-pr-kasko-calc-step' => '500',
  'invest-pr-kasko-calc-default' => '1000',
  'invest-pr-kasko-list-8-text'=> 'You conclude a vehicle insurance contract with any insurance company on standard terms.',
  'invest-pr-kasko-list-9-text' => '<a href="https://my.bravia.fund/register">You register at Bravia Fund</a> and get access to your personal account.',
  'invest-pr-kasko-list-10-text' => 'You open a deposit at Bravia Fund for any preferred term.',
  'invest-pr-kasko-list-11-text' => 'Provide Bravia Fund with the payment details and the Fund will pay the insurance for you.',
  'invest-pr-kasko-note'=> 'The filling process takes a couple of minutes and this is a favorable and convenient way to pay for the insurance.',
  'invest-pr-kasko-block-2-title'=>' You can get fully comprehensive insurance free of charge',
  'invest-pr-kasko-block-2-desc'=> 'By choosing the insurance company in your city or taking the advantage of our partner list'
]; 
