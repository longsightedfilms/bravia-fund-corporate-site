﻿<?php

return [
  'invest-main-title'           => 'Investment programs',
  'invest-main-desc'            => 'More than just deposits',
  'invest-main-desc-more'       => 'Investment programs from Bravia Fund - a set of simple investment instruments, our extensive experience in investment projects, and advantageous offers from our partners. Investment programs from Bravia Fund - additional benefits for our clients.',
  'invest-main-block-cat'       => 'Investment program',
  'invest-main-block-link'      => 'Learn more',
  'invest-main-block-1-title'   => 'Land plot and house in Moscow Region',
  'invest-main-block-1-desc'    => 'We transfer the land plot to your property immediately and build a house according to your project.',
  'invest-main-block-2-title'   => 'Apartment in Moscow City',
  'invest-main-block-2-desc'    => 'Rent an apartment or any other real estate in Moscow City at the rate of 0 USD per m<sup>2</sup> per year.',
  'invest-main-block-3-title'   => 'Car',
  'invest-main-block-3-desc'    => 'After opening a deposit, you get the ownership of the chosen car.',
  'invest-main-block-4-title'   => 'Apartment rent',
  'invest-main-block-4-desc'    => 'Rent of apartment or any other real estate in any region of Russia at the rate of 0 USD per m<sup>2</sup> per year.',
  'invest-main-block-5-title'   => 'Fully comprehensive insurance',
  'invest-main-block-5-desc'    => 'After opening a deposit, we will pay the fully comprehensive insurance for any vehicle for you.',
  'invest-main-block-6-title'   => 'Fitness',
  'invest-main-block-6-desc'    => 'After opening a deposit, we will pay for the annual membership card to any gym in your city.'
]; 
