﻿<?php

return [
  'partners-title'=>'Partners of Bravia Fund',
  'partners-desc'=>'Companies ensuring stable growth of Bravia Fund ',
  'partners-block-1-t'=>'Client payments in Russia ',
  'partners-block-2-t'=>'Bank cards for our clients',
  'partners-block-3-t'=>'Client payments in the European Union ',
  'partners-block-4-t'=>'Insurance of fund\'s financial liability',
  'partners-block-5-t'=>'Own cryptocurrency exchange, analytics and cryptocurrency trading department ',
  'partners-block-6-t'=>' Cryptocurrency trading ',
  'partners-block-7-t'=>'ICO rating agency',
  'partners-block-8-t'=>'Assistance with employment services'
]; 
