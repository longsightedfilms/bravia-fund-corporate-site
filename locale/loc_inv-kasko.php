<?php

return [
  'invest-pr-kasko-title' => 'Бесплатная страховка КАСКО для Вашего транспорта',
  'invest-pr-kasko-desc' => 'Сделайте Ваши ежегодные платежи по КАСКО бесплатными',
  'invest-pr-kasko-list-1-title' => 'Бесплатная страховка транспортного средства',
  'invest-pr-kasko-list-1-text' => 'Вам не нужно каждый год отдавать часть средств на страховку. Вы можете тратить эти деньги на себя или копить деньги на что-то.',
  'invest-pr-kasko-block-1-title' => 'Разместите депозит в Bravia Fund и мы возьмём на себя все вопросы по оплате',
  'invest-pr-kasko-block-1-desc' => 'Предоставьте нам реквизиты и мы ежегодно будем пересылать оплату страховки вместо Вас',
  'invest-pr-kasko-list-2-text' => 'В калькулятор, приведённом ниже, вводите стоимость страховки Вашего транспортного средства.',
  'invest-pr-kasko-list-3-text' => 'Выбираете удобный вариант размещения депозита в соответствующей сумме в Bravia Fund.',
  'invest-pr-kasko-list-4-text' => 'Размещаете депозит.',
  'invest-pr-kasko-list-5-text' => 'Bravia Fund ежегодно оплачивает Вашу страховку.',
  'invest-pr-kasko-block-left-1-text' => 'Вы заключаете договор со страховой компанией на стандартных условиях. Кладёте деньги на депозит в Bravia Fund и получаете ежемесячные выплаты, которые мы отправляем напрямую на погашение Вашего страхового платежа. Сделка с Bravia Fund закреплена официальным договором.',
  'invest-pr-kasko-block-left-2-text' => 'Вы имеете возможность досрочно вывести депозит, не дожидаясь истечения срока по договору. В этом случае сумма будет возвращена Вам незамедлительно и в полном объеме за вычетом процентов, выплаченных за этот срок по депозиту в счет оплаты страховки транспортного средства.',
  'invest-pr-kasko-calc-desc' => 'Установите стоимость страховки транспортного средства:',
  'invest-pr-kasko-calc-min' => '10000',
  'invest-pr-kasko-calc-max' => '300000',
  'invest-pr-kasko-calc-step' => '10000',
  'invest-pr-kasko-calc-default' => '180 000',
  'invest-pr-kasko-list-8-text' => 'Заключаете договор страховки ТС с любой страховой компанией на стандартных условиях.',
  'invest-pr-kasko-list-9-text' => '<a href="https://my.bravia.fund/register">Регистрируетесь в Bravia Fund</a> и получаете доступ к личному кабинету.',
  'invest-pr-kasko-list-10-text' => 'Размещаете депозит в Bravia Fund на удобный Вам срок.',
  'invest-pr-kasko-list-11-text' => 'Предоставляете Bravia Fund реквизиты для оплаты и Фонд оплачивает страховку за Вас.',
  'invest-pr-kasko-note' => 'Оформление займет минимум времени, но главное - это выгодный и удобный способ платить страховку.',
  'invest-pr-kasko-block-2-title' => 'Вы можете получить КАСКО бесплатно',
  'invest-pr-kasko-block-2-desc' => 'Выбрав страховую компанию вашего города или воспользоваться списком наших партнеров'
];